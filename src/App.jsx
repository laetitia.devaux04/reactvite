import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'

import Title from './components/Title'
import Contact from './components/Contact'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
  
      <Title/>
      <Contact fristName ="Milou" lastName ='tintin' statut ='online'/>
      <Contact fristName ="Emlie" lastName ='Jolie' statut ='online'/>
      <Contact fristName ="Mickey" lastName ='Mouse' statut ='offline'/>
      <Contact fristName ="Hades" lastName ='Dieu des Enfers' statut ='offline'/>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        react est une bibliothéque git
      </p>
    </div>
  )
}

export default App
